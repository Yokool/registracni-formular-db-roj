package com.roj;

import com.roj.database.DBConn;
import com.roj.files.AppFiles;

public class Main {

    public static void main(String[] args)
    {
        DBConn.db_init();
        RegistrationWindow window = new RegistrationWindow();
        window.create_window();
    }
}
