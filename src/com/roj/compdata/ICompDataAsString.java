package com.roj.compdata;

import com.roj.icompdata.DataKeyValue;

@FunctionalInterface
public interface ICompDataAsString
{
    DataKeyValue[] get_data();
}
