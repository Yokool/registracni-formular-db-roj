package com.roj.compdata;

import com.roj.expdata.DataKeyGroup;
import com.roj.icompdata.DataKeyValue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CompData
{

    public static void add_get_data_to_expanded_group(ArrayList<Object> group_expanded, DataKeyValue[] got_data)
    {

        for(DataKeyValue contained_data_el : got_data)
        {
            if(contained_data_el.key == null)
            {
                DataKeyValue[] wrapped_data = (DataKeyValue[]) contained_data_el.value;
                add_get_data_to_expanded_group(group_expanded, wrapped_data);
                continue;
            }

            group_expanded.add(contained_data_el.value);
        }
    }

    public static String comp_data_group_to_sep(ICompDataAsString[] group, String separator)
    {
        // 1 to 1 map into get_data() result
        ArrayList<DataKeyValue[]> group_expanded = new ArrayList<>();

        for(ICompDataAsString i_comp : group)
        {
            group_expanded.add(i_comp.get_data());
        }

        // get_data() expansion
        StringBuilder sep_string = new StringBuilder();

        ArrayList<Object> group_data_expanded = new ArrayList<>();

        for (DataKeyValue[] group_element_data : group_expanded)
        {
            add_get_data_to_expanded_group(group_data_expanded, group_element_data);
        }

        for(int i = 0; i < group_data_expanded.size(); ++i)
        {
            boolean last_of_data_group = i == group_data_expanded.size() - 1;

            Object data_element = group_data_expanded.get(i);
            sep_string.append(data_element);

            if(!last_of_data_group)
            {
                sep_string.append(separator);
            }
            else
            {
                sep_string.append("\n");
            }

        }

        /*
        for (int i = 0; i < group.length; ++i) {
            ICompDataAsString comp_data_el = group[i];
            DataKeyValue[] data = comp_data_el.get_data();

            boolean last_of_group = i == group.length - 1;

            for(int j = 0; j < data.length; ++j)
            {
                boolean last_of_data_group = j == data.length - 1;

                Object data_element = data[j].value;
                sep_string.append(data_element);


                if(!(last_of_group && last_of_data_group))
                {
                    sep_string.append(separator);
                }
                else
                {
                    sep_string.append("\n");
                }

            }
        }
         */

        return sep_string.toString();
    }


}
