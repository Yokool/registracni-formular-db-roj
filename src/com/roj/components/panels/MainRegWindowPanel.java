package com.roj.components.panels;

import com.roj.components.panels.base.RegistrationPanelScrollPane;
import com.roj.customconstr.GBConstr;

import javax.swing.*;
import java.awt.*;

public class MainRegWindowPanel extends JPanel
{

    private RegistrationPanelScrollPane registration_panel = new RegistrationPanelScrollPane();

    public MainRegWindowPanel()
    {
        component_init();
    }

    private void add_children()
    {
        JPanel left_padding = new JPanel();
        JPanel right_padding = new JPanel();

        GBConstr padding_constr_base = new GBConstr()

                .set_weight_y(100)
                .set_fill(GridBagConstraints.BOTH);

        GBConstr padding_constr_left = new GBConstr(padding_constr_base)
                .set_position(0, 0);

        GBConstr padding_constr_right = new GBConstr(padding_constr_base)
                .set_position(2, 0);

        add(left_padding, padding_constr_left.get_result());

        add(registration_panel, new GBConstr()
                .set_position(1, 0)
                .set_fill(GridBagConstraints.BOTH)
                .set_weight_y(1)
                .set_weight_x(60)
                .get_result()
        );

        add(right_padding, padding_constr_right.get_result());


    }

    private void component_init()
    {
        GridBagLayout layout = new GridBagLayout();

        layout.columnWidths = new int[] {
                50,
                0,
                50
        };

        setLayout(layout);
        add_children();
    }

}
