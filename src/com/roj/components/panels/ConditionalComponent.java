package com.roj.components.panels;

import com.roj.interfaces.AddComponentFunc;
import com.roj.interfaces.RemoveComponentFunc;

import javax.swing.*;
import java.awt.*;

public class ConditionalComponent extends JLabel
{

    private AddComponentFunc add_component_func;
    private RemoveComponentFunc remove_component_func;

    public ConditionalComponent(AddComponentFunc add_component_func, RemoveComponentFunc remove_component_func)
    {
        this.add_component_func = add_component_func;
        this.remove_component_func = remove_component_func;
        comp_init();
    }


    private void comp_init()
    {
        setForeground(Color.red);
    }

    private void add_component()
    {
        add_component_func.add_component();
    }

    private void remove_component()
    {
        remove_component_func.remove_component();
    }

    public void update_show_error(boolean hide_component)
    {
        if(hide_component)
        {
            remove_component();
            return;
        }

        add_component();
    }

}
