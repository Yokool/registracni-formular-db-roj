package com.roj.components.panels.base;

import com.roj.compdata.ICompDataAsString;
import com.roj.components.panels.*;
import com.roj.customconstr.GBConstr;
import com.roj.icompdata.DataKeyValue;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;
import java.util.List;

public class RegistrationPanel extends JPanel implements ICompDataAsString
{
    private final UserInfoPanel user_info_panel = new UserInfoPanel();
    private final BirthdayNumberPanel birthday_number_panel = new BirthdayNumberPanel();
    private final GenderNewsPanel gender_news_panel = new GenderNewsPanel(birthday_number_panel);
    private final SendInfoPanel send_info_panel = new SendInfoPanel(this);

    public RegistrationPanel()
    {
        comp_init();
    }

    private void comp_init()
    {
        GridBagLayout layout = get_layout();
        setLayout(layout);

        add_user_info_panel();

    }

    public ICompDataAsString[] get_data_group()
    {
        return new ICompDataAsString[] {
                user_info_panel,
                gender_news_panel,
                birthday_number_panel,
        };
    }

    @Override
    public DataKeyValue[] get_data() {
        return new DataKeyValue[] {
                new DataKeyValue(null, user_info_panel.get_data()),
                new DataKeyValue(null, gender_news_panel.get_data()),
                new DataKeyValue(null, birthday_number_panel.get_data()),
        };
    }


    public List<IOnRegisterCompValidation> get_validation_group()
    {
        ICompDataAsString[] group = get_data_group();
        return Arrays.stream(group).filter(
                (data_component) -> data_component instanceof IOnRegisterCompValidation
        ).map(
                (comp_validation_comp) -> (IOnRegisterCompValidation) comp_validation_comp
        ).toList();
    }

    private void add_user_info_panel()
    {
        GBConstr base_constr = new GBConstr()
                .set_fill(GridBagConstraints.BOTH);
        add(user_info_panel, new GBConstr(base_constr)
                .set_position(0, 0)
                .get_result()
        );

        add(gender_news_panel, new GBConstr(base_constr)
                .set_position(0, 1)
                .get_result()
        );

        add(birthday_number_panel, new GBConstr(base_constr)
                .set_position(0, 2)
                .get_result()
        );

        add(send_info_panel, new GBConstr(base_constr)
                .set_position(0, 3)
                .get_result()
        );

        add(new JPanel(), new GBConstr()
                .set_position(0, 4)
                .set_fill(GridBagConstraints.BOTH)
                .get_result()
        );
    }


    private GridBagLayout get_layout()
    {
        GridBagLayout layout = new GridBagLayout();

        layout.rowHeights = new int[] {
                180, // user info
                160, // gender box
                80,
                120
        };

        layout.rowWeights = new double[] {
                0.0,
                0.0,
                0.0,
                0.0,
                1.0
        };

        layout.columnWeights = new double[] {
                1.0
        };

        return layout;
    }

}
