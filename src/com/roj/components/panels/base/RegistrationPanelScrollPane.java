package com.roj.components.panels.base;

import com.roj.components.panels.base.RegistrationPanel;

import java.awt.*;

public class RegistrationPanelScrollPane extends ScrollPane
{
    private RegistrationPanel registration_panel = new RegistrationPanel();

    public RegistrationPanelScrollPane()
    {
        comp_init();
    }

    private void comp_init()
    {
        add_registration_panel();
    }

    private void add_registration_panel()
    {
        add(registration_panel);
    }
}
