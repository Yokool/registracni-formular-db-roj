package com.roj.components.panels;

import java.util.List;

public class ComponentValidation
{

    public static boolean run_validation(IOnRegisterCompValidation validation)
    {
        boolean is_comp_valid = validation.is_component_valid();

        if(is_comp_valid)
        {
            validation.on_is_valid();
        }
        else
        {
            validation.on_is_invalid();
        }

        return is_comp_valid;
    }

    public static boolean run_validation_group(List<IOnRegisterCompValidation> validation_group)
    {
        boolean success = true;
        for(IOnRegisterCompValidation validation : validation_group)
        {
            success &= run_validation(validation);
        }

        return success;
    }

}
