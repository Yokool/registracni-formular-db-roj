package com.roj.components.panels;

import com.roj.listeners.IValidationFunc;

public interface IOnRegisterCompValidation extends IValidationFunc
{
    void on_is_valid();
    void on_is_invalid();
}
