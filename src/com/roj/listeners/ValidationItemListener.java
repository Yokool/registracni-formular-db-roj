package com.roj.listeners;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class ValidationItemListener extends ValidationListener implements ItemListener {
    public ValidationItemListener(IValidationFunc validation_func) {
        super(validation_func);
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        perform_validation();
    }
}
