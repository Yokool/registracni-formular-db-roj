package com.roj.listeners;

import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class ValidationDocListener extends ValidationListener implements DocumentListener {

    public ValidationDocListener(IValidationFunc func)
    {
        super(func);
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        perform_validation();
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        perform_validation();
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        perform_validation();
    }
}
