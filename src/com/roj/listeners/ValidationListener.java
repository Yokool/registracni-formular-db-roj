package com.roj.listeners;

public class ValidationListener
{
    private IValidationFunc validation_func;

    protected void perform_validation()
    {
        validation_func.is_component_valid();
    }

    public ValidationListener(IValidationFunc validation_func)
    {
        this.validation_func = validation_func;
    }
}
