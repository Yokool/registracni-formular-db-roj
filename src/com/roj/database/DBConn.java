package com.roj.database;

import com.roj.DateStr;
import com.roj.expdata.DataKeyGroup;
import com.roj.expdata.ExportedDataKeys;
import com.roj.gender.GenderConst;
import com.roj.sqldata.IdUsernamePair;

import javax.xml.crypto.Data;
import java.sql.*;
import java.util.ArrayList;

public class DBConn
{

    private static Connection db_connection;

    public static Connection get_db_connection()
    {
        return db_connection;
    }

    public static void db_init()
    {
        init_db_connection();
        init_db_table();
    }

    public static void save_user(DataKeyGroup user_batch)
    {
        String username = user_batch.get_value(ExportedDataKeys.USERNAME);
        String password = user_batch.get_value(ExportedDataKeys.PASSWORD);
        String gender = user_batch.get_value(ExportedDataKeys.GENDER);
        boolean receive_newsletter = user_batch.get_value(ExportedDataKeys.RECEIVE_NEWSLETTER);
        String region = user_batch.get_value(ExportedDataKeys.REGION);

        String social_unique = user_batch.get_value(ExportedDataKeys.SOCIAL_UNIQUE);

        String birthdate_str = user_batch.get_value(ExportedDataKeys.BIRTHDATE);
        Date birthdate = DateStr.date_str_to_sql_date(birthdate_str);

        String sql = "INSERT INTO RegUsers (username, password, gender, receive_newsletter, region, birthdate, social_unique) VALUES (?, ?, ?, ?, ?, ?, ?) ";
        System.out.println("--- Přidávám data do DB ---");
        try {
            PreparedStatement statement = get_db_connection().prepareStatement(sql);
            statement.setString(1, username);
            statement.setString(2, password);
            statement.setString(3, gender);
            statement.setBoolean(4, receive_newsletter);
            statement.setString(5, region);
            statement.setDate(6, birthdate);
            statement.setString(7, social_unique);

            statement.executeQuery();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        user_username_xav_update(username);
    }


    public static void user_username_xav_update(String username_to_update)
    {
        ResultSet users = username_fetch(username_to_update);
        ArrayList<IdUsernamePair> ids_to_process = new ArrayList<>();

        try
        {
            while(users.next())
            {
                int id = users.getInt("user_id");
                String username = users.getString("username");

                ids_to_process.add(new IdUsernamePair(id, username));
            }
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }

        if(ids_to_process.size() == 0)
        {
            System.err.println("WARN: Found no users for xav update with username " + username_to_update);
            return;
        }

        xav_update_ids(ids_to_process);
    }

    private static void xav_update_ids(ArrayList<IdUsernamePair> ids_to_process)
    {
        for(IdUsernamePair id_username_pair : ids_to_process)
        {
            xav_update_id(id_username_pair);
        }
    }

    private static void xav_update_id(IdUsernamePair id_username_pair)
    {
        System.out.println("Přidávám " + DatabaseConst.DB_USERNAME_PREPED + " před uživatele (" + id_username_pair.id + ", " + id_username_pair.username + ")");
        String sql = "UPDATE RegUsers SET username=? WHERE user_id=? LIMIT 1";

        try
        {
            PreparedStatement statement = get_db_connection().prepareStatement(sql);
            statement.setString(1, DatabaseConst.DB_USERNAME_PREPED + id_username_pair.username);
            statement.setInt(2, id_username_pair.id);
            statement.execute();
        }
        catch (SQLException e)
        {
            e.printStackTrace();
        }
    }

    public static ResultSet username_fetch(String username)
    {
        String sql = "SELECT * FROM RegUsers WHERE username=?";
        try
        {
            PreparedStatement statement = get_db_connection().prepareStatement(sql);
            statement.setString(1, username);
            return statement.executeQuery();
        } catch (SQLException e)
        {
            e.printStackTrace();
            throw new RuntimeException("Something failed while trying to fetch user by username " + username);
        }

    }

    private static void init_db_table()
    {
        try {
            get_db_connection().prepareStatement("CREATE DATABASE IF NOT EXISTS RegistrationSystem").executeQuery();
            get_db_connection().prepareStatement("USE RegistrationSystem").executeQuery();
            get_db_connection().prepareStatement("CREATE TABLE IF NOT EXISTS RegUsers(" +
                    "user_id int PRIMARY KEY AUTO_INCREMENT," +
                    "username varchar(255)," +
                    "password varchar(255)," +
                    "gender varchar(1)," +
                    "receive_newsletter BIT," +
                    "region varchar(255)," +
                    "birthdate DATE," +
                    "social_unique varchar(4))"
            ).executeQuery();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private static void init_db_connection()
    {
        try
        {
            db_connection = DriverManager.getConnection(DatabaseConst.DB_IP);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

}
