package com.roj;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateStr
{

    public static String date_to_string(Date date)
    {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(date);

        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH) + 1;
        int year = calendar.get(Calendar.YEAR);

        return String.format("%d-%d-%d", year, month, day);
    }

    public static java.sql.Date date_str_to_sql_date(String date_str)
    {
        String[] date_split = date_str.split("-");
        int year = Integer.parseInt(date_split[0]);
        int month = Integer.parseInt(date_split[1]) - 1;
        int day = Integer.parseInt(date_split[2]);
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.set(year, month, day);
        return new java.sql.Date(calendar.getTime().getTime());
    }

}
