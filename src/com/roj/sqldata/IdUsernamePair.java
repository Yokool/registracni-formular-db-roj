package com.roj.sqldata;

public class IdUsernamePair
{

    public int id;
    public String username;

    public IdUsernamePair(int id, String username)
    {
        this.id = id;
        this.username = username;
    }

}
