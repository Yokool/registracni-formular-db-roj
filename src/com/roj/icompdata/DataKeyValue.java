package com.roj.icompdata;

public class DataKeyValue
{
    public DataKeyValue(String key, Object value)
    {
        this.key = key;
        this.value = value;
    }

    public String key;
    public Object value;
}
