package com.roj.arrays;

import java.util.ArrayList;
import java.util.Arrays;

public class ArrayHelper
{

    public static Character[] box_char_array(char[] char_array)
    {
        Character[] boxed_array = new Character[char_array.length];

        for(int i = 0; i < char_array.length; ++i)
        {
            boxed_array[i] = char_array[i];
        }

        return boxed_array;
    }

    public static char[] unbox_char_array(Character[] boxed_char_array)
    {
        char[] unboxed_array = new char[boxed_char_array.length];

        for(int i = 0; i < boxed_char_array.length; ++i)
        {
            unboxed_array[i] = boxed_char_array[i];
        }

        return unboxed_array;
    }

}
