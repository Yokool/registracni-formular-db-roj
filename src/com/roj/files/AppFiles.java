package com.roj.files;

import java.io.*;
import java.net.URISyntaxException;

public class AppFiles
{
    public static File get_launch_directory()
    {
        try {
            return new File(AppFiles.class.getProtectionDomain().getCodeSource().getLocation()
                    .toURI()).getParentFile();
        } catch (URISyntaxException e) {
            throw new RuntimeException("Something went wrong during the acquisition of the launch directory");
        }
    }

    public static final File FORM_DATA_OUTPUT_FILE = new File(get_launch_directory() + "\\csv_data.csv");

    public static void assert_fdata_output_file_existence()
    {
        try {
            FORM_DATA_OUTPUT_FILE.createNewFile();
        } catch (IOException e) {
            throw new RuntimeException("Something went wrong during the creation of the fdata file.");
        }
    }

    public static void add_data_to_output_file(String data)
    {
        assert_fdata_output_file_existence();
        try(PrintWriter pw = new PrintWriter(new FileOutputStream(FORM_DATA_OUTPUT_FILE, true))) {
            pw.write(data);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
