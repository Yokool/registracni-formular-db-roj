package com.roj;

import com.roj.components.panels.MainRegWindowPanel;
import com.roj.sizes.StandardSizes;

import javax.swing.*;

public class RegistrationWindow extends JFrame
{
    private MainRegWindowPanel main_window_panel = new MainRegWindowPanel();

    public static final String REGISTRATION_WINDOW_NAME = "Registrace - Roj";

    public RegistrationWindow()
    {

    }

    public void create_window()
    {
        setTitle(REGISTRATION_WINDOW_NAME);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(StandardSizes.HD);
        setVisible(true);

        add_main_panel();
    }

    private void add_main_panel()
    {
        add(main_window_panel);
    }

}
