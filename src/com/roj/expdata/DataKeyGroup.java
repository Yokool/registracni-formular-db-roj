package com.roj.expdata;

import com.roj.icompdata.DataKeyValue;

import java.util.HashMap;

public class DataKeyGroup
{
    private final HashMap<String, Object> data_map = new HashMap<>();

    public <T> T get_value(String key)
    {
        return (T) data_map.get(key);
    }

    public void remove_key(String key)
    {
        data_map.remove(key);
    }

    public DataKeyGroup(DataKeyValue[] data)
    {
        process_data_as_key_values(data);
    }

    private void process_data_as_key_values(DataKeyValue[] data)
    {
        for (DataKeyValue data_element : data)
        {
            process_data_key_entry(data_element);
        }
    }

    private void process_data_key_entry(DataKeyValue data_element)
    {
        String key = data_element.key;
        if(key == null)
        {
            DataKeyValue[] wrapped_group = (DataKeyValue[]) data_element.value;
            process_data_as_key_values(wrapped_group);
            return;
        }
        data_map.put(key, data_element.value);
    }

}
