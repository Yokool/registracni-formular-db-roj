package com.roj.expdata;

public class ExportedDataKeys
{

    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String BIRTHDATE = "birthdate";
    public static final String SOCIAL_YEAR = "social_year";
    public static final String SOCIAL_MONTH = "social_month";
    public static final String SOCIAL_DAY = "social_day";
    public static final String SOCIAL_UNIQUE = "social_unique";
    public static final String GENDER = "gender";
    public static final String RECEIVE_NEWSLETTER = "receive_newsletter";
    public static final String REGION = "region";
}
