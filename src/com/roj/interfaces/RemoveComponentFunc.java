package com.roj.interfaces;

@FunctionalInterface
public interface RemoveComponentFunc
{
    void remove_component();
}
