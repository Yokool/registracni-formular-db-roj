package com.roj.interfaces;

@FunctionalInterface
public interface AddComponentFunc
{
    void add_component();
}
