package com.roj.symbols;

import com.roj.arrays.ArrayHelper;
import org.xml.sax.ext.LexicalHandler;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;

public class Symbols
{


    public static final char[] ALPHABET = {
            'a',
            'b',
            'c',
            'd',
            'e',
            'f',
            'g',
            'h',
            'i',
            'j',
            'k',
            'l',
            'm',
            'n',
            'o',
            'p',
            'q',
            'r',
            's',
            't',
            'u',
            'v',
            'w',
            'x',
            'y',
            'z',
            'ě',
            'š',
            'č',
            'ř',
            'ž',
            'ý',
            'á',
            'í',
            'é',
            'ů'
    };

    public static final char[] NUMBERS = {
            '0',
            '1',
            '2',
            '3',
            '4',
            '5',
            '6',
            '7',
            '8',
            '9'
    };

    public static final Character[] NUMBERS_BOXED = ArrayHelper.box_char_array(NUMBERS);

    public static final char[] SYMBOLS = {
            '#',
            '@',
            '^',
            '&',
            '/',
            '(',
            ')',
            '"',
            '\'',
            '-',
            '?',
            '!',
            '%',
            ':',
            '+',
            '_',
            '[',
            ']',
            '.',
            ';',
            '~'
    };

    public static final Character[] SYMBOLS_BOXED = ArrayHelper.box_char_array(SYMBOLS);

    public static final Character[] ALPHABET_BOXED = ArrayHelper.box_char_array(ALPHABET);

    public static char[] ALPHABET_UPPERCASE = new char[ALPHABET.length];

    static
    {
        for (int i = 0; i < ALPHABET.length; ++i)
        {
            ALPHABET_UPPERCASE[i] = Character.toUpperCase(ALPHABET[i]);
        }
    }

    public static Character[] ALPHABET_UPPERCASE_BOXED = ArrayHelper.box_char_array(ALPHABET_UPPERCASE);

    public static char[] ALLOWED_FIELD_SYMBOLS;

    public static Character[] ALLOWED_FIELD_SYMBOLS_BOXED;

    static
    {
        LinkedList<Character> concat = new LinkedList<>();
        concat.addAll(List.of(ALPHABET_BOXED));
        concat.addAll(List.of(ALPHABET_UPPERCASE_BOXED));
        concat.addAll(List.of(NUMBERS_BOXED));
        concat.addAll(List.of(SYMBOLS_BOXED));

        ALLOWED_FIELD_SYMBOLS_BOXED = new Character[concat.size()];
        ALLOWED_FIELD_SYMBOLS_BOXED = concat.toArray(ALLOWED_FIELD_SYMBOLS_BOXED);

        ALLOWED_FIELD_SYMBOLS = ArrayHelper.unbox_char_array(ALLOWED_FIELD_SYMBOLS_BOXED);
    }

    public static boolean is_string_legal(String str)
    {
        List<Character> allowed_symbols = Arrays.stream(ALLOWED_FIELD_SYMBOLS_BOXED).toList();
        boolean contains_illegal = str.chars().anyMatch((character) -> !allowed_symbols.contains((char)character));
        return !contains_illegal;
    }

}
